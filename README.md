# Project 5: Brevet Time Calculator (with Mongo)
### Author: Ian Parish Email: iparish@cs.uoregon.edu
### Developed for CIS322 F20, University of Oregon

---

This is an implementation of a calculator used to determine the opening and closing times of control points as determined by the user for a brevet for _randonneurs_ (see (https://en.wikipedia.org/wiki/Randonneuring) for more infomation on randonneuring). Its implementation is based on the algorithm outlined at (https://rusa.org/pages/acp-brevet-control-times-calculator), and the rules established at the *Randonneurs USA* website (https://rusa.org/pages/rulesForRiders).

For this iteration of the project, the ability to save to and display from a database using MongoDB was implemented. Find the 'Submit' and 'Display' buttons at the bottom of the page.

---

## Requirements:
 * Docker
 * a valid credentials.ini file

To run:

`$ docker-compose up`
Yep, that's all you need!

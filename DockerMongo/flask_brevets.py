"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
#app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
#For Windows testing
#client = MongoClient("172.17.0.2", 27017)
db = client.tododb
collection = db.controls

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    
    date = request.args.get('date', type=str)
    time = request.args.get('time', type=str)
    dt = date + " " + time + ":00"
    arrowdt = arrow.get(dt, 'YYYY-MM-DD HH:mm:ss')
    dist = request.args.get('dist', type=float)
    
    open_time = acp_times.open_time(km, dist, arrowdt)
    close_time = acp_times.close_time(km, dist, arrowdt)
    app.logger.debug("close: {}".format(close_time))
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


@app.route('/_submit')
def _submit():
    # Get the entries from the page
    entries = request.form.getlist('entries[]')
    app.logger.debug("entries from page: {}".format(entries))
    # Iterate and split entries to add to db
    for entry in entries:
        if entry is not ",,":
            tmp = entry.split(',')
            item_doc = {
                'km': tmp[0],
                'open': tmp[1],
                'close': tmp[2],
            }
            collection.insert(item_doc)
    return flask.render_template('calc.html')



@app.route('/_display')
def _display():
    # Format from app.py example
    _items = db.collection.find()
    items = [item for item in _items]

    return flask.render_template('controlelist.html', items=items)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")

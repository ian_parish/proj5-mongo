"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
from math import floor

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

# Dict of time limits as defined by the RUSA Rules page
timeLims = {200: [13, 30], 300: [20, 0], 400: [27, 0], 600: [40, 0], 
               1000: [75, 0]}

# List of lists of control min/max speeds (as determined by the range ceiling)
ctrlRng = [ [200, 15, 34], [400, 15, 32], [600, 15, 30], [1000, 11.428, 28],
               [1300, 13.333, 26] ]


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    # Check for proper range
    if control_dist_km > brevet_dist_km:
       if (control_dist_km) <= (brevet_dist_km * 1.2):
          control_dist_km = brevet_dist_km
       else:
          return -1

    diff = 0
    remaining = control_dist_km
    lastRng = 0
    time = 0
    i = 0
    loop = True

    while loop:
       if control_dist_km > ctrlRng[i][0]:
          diff = ctrlRng[i][0] - lastRng
          time += diff / ctrlRng[i][2]
          lastRng = ctrlRng[i][0]
          remaining -= diff
          i += 1
       else:
          time += remaining / ctrlRng[i][2]
          loop = False
  
    hr = floor(time)
    min = int(round(((time % 1) * 60), 0))

    # Add the calculated time to the initial start time
    start = arrow.get(brevet_start_time)
    start = start.shift(hours = hr, minutes = min)
    return start.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    # Check for proper range
    if control_dist_km > brevet_dist_km:
       if (control_dist_km) <= (brevet_dist_km * 1.2):
          control_dist_km = brevet_dist_km
       else:
          return -1
    
    # Use default time limit rules for final control point
    if control_dist_km == brevet_dist_km:
       close = arrow.get(brevet_start_time)
       close = close.shift(hours = timeLims[control_dist_km][0],
                               minutes = timeLims[control_dist_km][1])
       return close.isoformat()

    loop = True
    remaining = control_dist_km
    lastRng = 0
    time = 0
    diff = 0
    i = 0

    print(remaining)
    while (loop):
       if control_dist_km > ctrlRng[i][0]:
          diff = ctrlRng[i][0] - lastRng
          time += diff / ctrlRng[i][1]
          lastRng = ctrlRng[i][0]
          remaining -= diff
          i += 1
       else:
          time += remaining / ctrlRng[i][1]
          loop = False

    hr = floor(time)
    min = int(round(((time % 1) * 60), 0))

    close = arrow.get(brevet_start_time)
    close = close.shift(hours = hr, minutes = min)

    return close.isoformat()